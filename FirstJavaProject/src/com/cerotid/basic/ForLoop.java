package com.cerotid.basic;

public class ForLoop {
	
	/*
	 * public static void main(String[] args) {
	 * 
	 * for(Initilazation condition; testing condition) {
	 * 
	 * Increment/Decrement }
	 * 
	 * statements(s)
	 * 
	 * }
	 */

	
	public static void main(String[] args) {
		//for loop begains when x= 5 and runs till  x <=20
		for (int x = 5; x <= 20; x++) {
			System.out.println("Value of x:" + x);
		}
	}
	
	

}
