package com.cerotid.basic;

public class WhileLoop {

	//main 
	public static void main(String[] args) {
		
		
		//While loop 
		//Boolean = True / False
		
		
		//Program to show while loop 
		int x = 2; 
		
		//Exit loop onece x is greaer than 5
		while(x <=10) {
			System.out.println("Value of x:" + x);
			//increment the value of x till we reach 5 
			//x = x + 1;
			x++;
		}
		
		
	}

}
