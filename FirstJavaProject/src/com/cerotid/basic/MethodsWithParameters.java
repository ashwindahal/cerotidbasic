package com.cerotid.basic;

import org.omg.CosNaming.NamingContextExtPackage.AddressHelper;

public class MethodsWithParameters {

	public static void main(String[] args) {

		// 1st method
		// myName("Ashwin ");

		// 2nd method
		// (5, 5);

		// 3rd Method
		// NameAdd("Ashwin", 20, 20);

		// 4th method
		// NameAddButBetter();

		// 5th method with return type
		// System.out.println(returnInt(5));

		// add
		add(5, 5);
		System.out.println(sub(5, 5));
		System.out.println(mul(5, 5));
		System.out.println(divide(5, 5));

	}

	// 1
	static void myName(String name) {
		System.out.println(name + "Is in Java/QA Class");

	}

	// 2
	// Addition
	static void add(int i, int x) {
		int sum = i + x;
		System.out.println(sum);

	}

	// 3
	static void NameAdd(String name, int i, int x) {
		System.out.println("Hello " + name);
		int sum = i + x;
		System.out.println(sum);

	}

	// 4
	static void NameAddButBetter() {
		myName("Ashwin");
		add(50, 50);
	}

	// 5
	// Return Type
	// Void: A void method has no return type

	static int returnInt(int x) {
		x = x * 5;
		return x;
	}

	// 6
	// Return type with two parameters
	// Multiplication
	static int mul(int x, int y) {
		int z = x * y;
		return z;
	}

	// Divsion
	static int divide(int x, int y) {
		int z = x / y;
		return z;
	}

	// Subtract
	static int sub(int x, int y) {
		return x - y;
	}
}
