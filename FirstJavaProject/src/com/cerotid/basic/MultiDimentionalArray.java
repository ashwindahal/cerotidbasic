package com.cerotid.basic;

public class MultiDimentionalArray {

	public static void main(String[] args) {

		// Syntax of multi dimentiional arrays
		/*
		 * data_type[1st dimention][2nd dimention][3rd dimention][nth dimention]
		 * array_name = new data_type[size1][size2][nth size];
		 * 
		 * data_type = Type of data to store dimension = the dimension of the array
		 * created array_name = name or vairable name; size1,siz2,nthsize = sizes of the
		 * dimentions;
		 */

		/*
		 * //Two Dimentional Array Example String[][] twoDimentionalArray = new
		 * String[10][20]; int[][] twoDimentionalArrayInteger = new int[10][20];
		 * 
		 * //Three Dimentional Arrays int [][][] threeD_Array = new int [10][20][30];
		 * 
		 * 
		 * 
		 * //2D Example
		 * 
		 * int[][] arr1 = new int[10][20]; arr1[0][0] = 1;
		 * System.out.println("arr[0][0]= " + arr1[0][0]);
		 * 
		 * String[][] twoDString_Array = new String[10][20]; twoDString_Array[0][0] =
		 * "Apple"; System.out.println(twoDString_Array[0][0]);
		 */

		/*
		 * int[][] arr2 = { { 1, 2 }, { 3, 4 } };
		 * 
		 * for (int i = 0; i < arr2.length; i++) { for (int j = 0; j < arr2.length; j++)
		 * { System.out.println("arr[" + i + "][" + j + "] = " + arr2[i][j]); }
		 * 
		 * }
		 */
		
		//Declaring three 3D Array
		String[][][] threeD_Array = {{{"Travis Scott","Eminem"},{"nicky minaj","Adele"}},{{"Arthun Gunn","Katy Perry"},{"Taylor S","MJ"}}};
		
		for (int i = 0; i < threeD_Array.length; i++) {
			for (int j = 0; j < threeD_Array.length; j++) {
				for (int j2 = 0; j2 < threeD_Array.length; j2++) {
					System.out.println("Artists: arr[" + i + "][" + j + "][" + j2 + "]= " + threeD_Array[i][j][j2]);
				}
			}	
		}
	}

}
