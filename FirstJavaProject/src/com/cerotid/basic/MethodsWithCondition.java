package com.cerotid.basic;

public class MethodsWithCondition {

	public static void main(String[] args) {
		validateAgeToDrink(21);
	}
	static void validateAgeToDrink(int age) {
		// less then 18 then you are child
		// greater then or equal to 18 you are adult
		if (age >= 21) {
			System.out.println("You are old enough, enjoy your beer");
		} else {
			System.out.println("Sorry no beer for you");
		}
	}
}
