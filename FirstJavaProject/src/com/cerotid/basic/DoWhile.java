package com.cerotid.basic;

public class DoWhile {
	
	public static void main(String[] args) {
	
		/*
		 * do {
		 * 
		 * } while(Condition);
		 */
		
		
		
		//Program to show d0-while loop 
	
		int x = 20; 
		do {
			//Line will printed even 
			System.out.println("Value of x:" + x);
			x++;
			
		}while(x < 30);
	
		
	}

}
