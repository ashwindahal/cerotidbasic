package com.cerotid.basic;

public class Arrays {

	public static void main(String[] args) {

		/*
		 * String[] DaysOfWeek = { "Monday", "Tuesday", "Wednesday", "Thurdsay",
		 * "Friday", "Saturday", "Sunday" }; System.out.println(DaysOfWeek[2]);
		 * 
		 * int[] NumOfMonths = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
		 * System.out.println(NumOfMonths[9]);
		 * 
		 * DaysOfWeek[2] = "Humpday";
		 * 
		 * System.out.println(DaysOfWeek[2]);
		 */
		/*
		 * //Array length String[] celebs = {"Ellen D", "jennifer aniston", "tom hank",
		 * "Steve carrol", "chris evans", "Brad Pitt",
		 * "Kanye West","jimmy kimmel","Ariana Grande", "Kylie Jenner"}; for (int i = 0;
		 * i < celebs.length; i++) { System.out.println(celebs[i]);
		 * 
		 * if(celebs[i].equalsIgnoreCase("jennifer aniston")){ System.out.println("Hi "
		 * + celebs[i] + " " + "How you doing?"); }
		 * 
		 * }
		 */

		String[] celebs = { "Travis Scott", "Kim Kardashian", "Kylie Jenner", "Jennifer Aniston", "Ellen Degeneres",
				"Chris Hensworth", "Jennifer Lawrence", "kevin Hart", "Dwayne D Johnson", "Vin Diesel" };
		System.out.println(celebs.length);
		for (int i = 0; i < celebs.length; i++) {
			System.out.println(celebs[i]);
			if (celebs[i].contains("Travis Scott")) {
				System.out.println("Hi " + celebs[i] + ", Are you still single and ready to mingle?");
			}

		}

		// 2d array
		// int[][] twoDimentional_array = new int[10][20];
		// twoDimentional_array[9][19] = 200;

		// System.out.println(twoDimentional_array[0][0]);
		// System.out.println(twoDimentional_array[9][18]);

		// int[][][] threeDimentional_Array = new int

		/*
		 * String[][][] threeDimentional_array = { { { "Car", "TV", "Fan" }, { "Hat",
		 * "Movie", "Love" }, { "Table", "chair", "Star" }, { "Game", "Ship", "Ball" } }
		 * };
		 * 
		 * for (int i = 0; i < threeDimentional_array.length; i++) { for (int j = 0; j <
		 * 4; j++) { for (int x = 0; x < 3; x++) { System.out.println("arr[" + i + "]["
		 * + j + "][" + x + "] = " + threeDimentional_array[i][j][x]); } }
		 * 
		 * }
		 */

	}

}
