package com.cerotid.basic;

public class Methods {

	// Methods is a block of code that runs only when called
	// You can pass diffrent data --> known as Parameters
	// Methods are used to perform certain actions and these actions are known as
	// functions
	
	public static void main(String[] args) {
		
		//Calling Method
		PrintExecuting();
		PrintExecuting();
		PrintExecuting();
		myMethod();
		
	}
	
	
	
	// Created a method
	static void myMethod() {
		System.out.println("I am in Java/QA Class");
	}
	static void PrintExecuting() {
		System.out.println("I just got executed");
	}
}
