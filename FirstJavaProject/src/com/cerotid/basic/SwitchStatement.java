package com.cerotid.basic;

public class SwitchStatement {

	public static void main(String[] args) {

		/*
		 * switch (key) { case value:
		 * 
		 * break;
		 * 
		 * default: break; }
		 */

		int day = 0;
		switch (day) {
		case 1:
			System.out.println("It monday");
			break;

		case 2:
			System.out.println("Its tuesday");
			break;

		case 3:
			System.out.println("its wednesday");
			break;

		case 4:
			System.out.println("its thursday");
			break;

		case 5:
			System.out.println("Its firday");
			break;

		case 6:
			System.out.println("its saturday");
			break;

		case 7:
			System.out.println("its sunday");
			System.out.println("this is a java intro class");
			break;
		
		default:
			System.out.println("Please enter a vild week number");
			
		}
	}

}
