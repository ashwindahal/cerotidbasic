package com.cerotid.basic;

//Primative Data Types 
public class DataTypes {

	public static void main(String[] args) {

		/// ---------------------------------------------------To Create a Variable we
		/// need below data types
		// Primative Data Types
		// byte - Stores whole numbers from -128 to 127
		// short - Stores whole numbers frm -32000 to 32000
		// int
		// long
		// float
		// double
		// boolean
		// char

		// Non- Primative Data Types
		// String

		// How to create a Variable
		byte myByte = 127;
		short myShort = 4000;
		int myInt = 5000;
		long myLong = 800000000;
		float myFloat = 5.75f;
		double myDouble = 2.5;
		boolean myBool = true;
		boolean myBool2 = false;
		char myChar = 'A';

		String myString = "This is a string";

		// Division
		int dividedNumer = myInt / myByte;
		int a = 2;
		int b = 2;

		int mySum = a + b;
		System.out.println(mySum);
		System.out.println(a + b);

		String fistWord = "Java";
		String secondWord = "Programming";
		String thirdWord = "!"; 
		
		System.out.println(fistWord + " " +  secondWord + thirdWord);
		
	}

}
