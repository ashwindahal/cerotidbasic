package com.cerotid.basic;

import java.util.Scanner;

public class StudentGrade {

	public static void main(String[] args) {

		// Create a Array of 5 to store student Subjects
		int subjects[] = new int[5];

		// Store Average Grades
		double sum = 0;
		double average = 0;

		// Create Reader to read input from console
		Scanner scanner = new Scanner(System.in);

		// Loop through input and get average
		for (int i = 0; i < subjects.length; i++) {
			System.out.println("Please Enter Course " + (i + 1) + " Grade:");
			subjects[i] = scanner.nextInt();
			sum = sum + subjects[i];
		}
		// Close Scanner
		scanner.close();

		// Get Average of Grades
		average = sum / 5;

		// Send Average to Method
		GradeCalculator(average);

	}

	static void GradeCalculator(double x) {
		if (x >= 90) {
			System.out.println("Average Grade: A");
		} else if (x >= 80 && x <= 89) {
			System.out.println("Average Grade: B");
		} else if (x >= 70 && x <= 79) {
			System.out.println("Average Grade: C");
		} else if (x >= 60 && x <= 69) {
			System.out.println("Average Grade: D");
		} else {
			System.out.println("Average Grade: F");
		}
	}

}
