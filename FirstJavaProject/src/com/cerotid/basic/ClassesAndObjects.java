package com.cerotid.basic;

public class ClassesAndObjects{
	
	//Class level Variables 
	int students = 24;
	String name = "QA";
	
	public static void main(String[] args) {
		
		ClassesAndObjects myClass = new ClassesAndObjects(); 
		System.out.println(myClass.students);
		System.out.println(myClass.name);
		
		Java myJavaClass = new Java(); //Object 1
		CPlusPlus myCPlusPlus = new CPlusPlus();  //Object 2
		
		
		//Printing Values form Two seperate classes 
		System.out.println(myJavaClass.isOOPS);
		System.out.println(myCPlusPlus.isOOPS);
		
	}
	
}

class Java{
	
	boolean isOOPS = true; 
	
}

class CPlusPlus{
	boolean isOOPS = true; 
	
}
