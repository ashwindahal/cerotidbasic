package com.cerotid.basic;

public class ifCondition {
	
	public static void main(String[] args) {
		
		String animal1 = "Dog";
		String animal2 = "Cat"; 
		
		int a = 2;
		int b = 2;
		
		int mySum = a + b;
		
		
		//If Condition Example 
		
		//&& - and operator 
		if (animal1 == animal2 && mySum == 4) {
			System.out.println("True");
		}else if (animal1 != animal2) {
			System.out.println("False- Animal 1 does not equal animal 2");
		}
		
		//||- or operator 

		if (animal1 == animal2 || mySum == 4) {
			System.out.println("True");
		}else if (animal1 != animal2) {
			System.out.println("False- Animal 1 does not equal animal 2");
		}
		

		
	}
}
